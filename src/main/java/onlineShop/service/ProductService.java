package onlineShop.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import onlineShop.entity.Product;
import onlineShop.repository.ProductRepository;

import java.util.List;

@Service
public class ProductService {
    @Autowired
    private ProductRepository productRepository;

    public List<Product> getAllProducts(){
        return productRepository.findAll();
    }

    public List<Product> getAllByCategory(String categorie){
        return  productRepository.findByCategory(categorie);
    }
    public Product getProduct(Integer id){
        return productRepository.getOne(id);
    }

    public void delete(Product product) {
        productRepository.delete(product);
    }
}
