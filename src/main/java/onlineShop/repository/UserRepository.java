package onlineShop.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import onlineShop.entity.User;

public interface UserRepository extends JpaRepository<User, Integer> {
    User findByUsername(String username);
}
