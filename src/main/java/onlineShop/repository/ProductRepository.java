package onlineShop.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import onlineShop.entity.Product;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ProductRepository extends JpaRepository<Product, Integer> {

    @Query("SELECT p from Product p where p.categories LIKE %?1%")
    List<Product> findByCategory(String categorie);
}
