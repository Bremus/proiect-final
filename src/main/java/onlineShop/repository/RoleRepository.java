package onlineShop.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import onlineShop.entity.Role;

public interface RoleRepository extends JpaRepository<Role, String> {
}
