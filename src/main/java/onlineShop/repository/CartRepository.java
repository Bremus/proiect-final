package onlineShop.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import onlineShop.entity.Cart;
import onlineShop.entity.User;

import java.util.List;

public interface CartRepository extends JpaRepository<Cart, Integer> {
    List<Cart> findByUser(User user);
}
