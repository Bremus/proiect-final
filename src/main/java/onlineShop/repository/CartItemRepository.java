package onlineShop.repository;

import onlineShop.entity.Cart;
import onlineShop.entity.CartItem;
import onlineShop.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CartItemRepository extends JpaRepository<CartItem, Integer> {
    List<CartItem> findByProduct(Product product);

    List<CartItem> findByCart(Cart cart);
}