package onlineShop.controller;

import onlineShop.entity.Product;
import onlineShop.repository.CartItemRepository;
import onlineShop.repository.ProductRepository;
import onlineShop.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class AdminController {

    @Autowired
    ProductRepository productRepository;

    @Autowired
    private ProductService productService;

    @Autowired
    private CartItemRepository cartContainer;

    // POST: Save product
    @PostMapping("/products")
    @PostAuthorize("hasAnyAuthority('ADMIN')")
    public String productSave(Model model, //
                              @ModelAttribute("product") Product products, //
                              BindingResult result, //
                              final RedirectAttributes redirectAttributes) {

        productRepository.save(products);
        return "redirect:/products";
    }

    @GetMapping("/products")
    @PostAuthorize("hasAnyAuthority('ADMIN')")
    public String product(Model model) {
        model.addAttribute("products", new Product());
        model.addAttribute("produseFemei", productService.getAllByCategory("FEMEI"));
        model.addAttribute("produseBarbati", productService.getAllByCategory("BARBATI"));
        return "products";
    }

    @GetMapping("/products/{id}")
    public String deleteProduct(@PathVariable Integer id) {
        Product theProduct = productRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Product not Found with id: " + id));
        productRepository.delete(theProduct);
        return "redirect:/products";
    }

}