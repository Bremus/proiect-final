package onlineShop.entity;

import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;

@ToString
@Entity
@Table(name = "cart")
public class Cart implements Serializable {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "userId", referencedColumnName = "id", nullable = false)
    @ToString.Exclude
    private User user;

    @Column(name = "delivery_address")
    private String deliveryAddress;

    @Column(name = "phone")
    private String phone;

    @Column(name = "fname")
    private String fname;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getDeliveryAddress() {
        return deliveryAddress;
    }

    public void setDeliveryAddress(String deliveryAddress) {
        if (!deliveryAddress.isEmpty()) {
            this.deliveryAddress = deliveryAddress;
        }
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        if (!phone.isEmpty()) {
            this.phone = phone;
        }
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        if (!fname.isEmpty()) {
            this.fname = fname;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Cart cart = (Cart) o;

        if (!id.equals(cart.id)) return false;
        if (!user.equals(cart.user)) return false;
        if (!deliveryAddress.equals(cart.deliveryAddress)) return false;
        if (!phone.equals(cart.phone)) return false;
        return fname.equals(cart.fname);
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + user.hashCode();
        result = 31 * result + deliveryAddress.hashCode();
        result = 31 * result + phone.hashCode();
        result = 31 * result + fname.hashCode();
        return result;
    }
}
