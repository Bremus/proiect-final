function toggleHomeDelivery(checkbox) {
    if(checkbox.checked) {
        document.getElementById("store").checked=false;
        document.getElementById("homedelivery").style.display="block";
    } else {
        document.getElementById("store").checked=true;
        document.getElementById("homedelivery").style.display="none";
    }
}

function toggleStorePickup(checkbox) {
    if(checkbox.checked) {
        document.getElementById("home").checked=false;
        document.getElementById("homedelivery").style.display="none";
    } else {
        document.getElementById("home").checked=true;
        document.getElementById("homedelivery").style.display="block";
    }
}